[![Netlify Status](https://api.netlify.com/api/v1/badges/3ffea3b6-4ce2-485d-91a8-9539ee7be32c/deploy-status)](https://app.netlify.com/sites/network-platform-docs/deploys)

# Network Platform Documentation

This repository aims at centralizing all the Network Platform documentation, including, for example, platform goals, design choices, and deployment adresses.

The documentation is under construction. The latest documentation is hosted at this address: <https://network-platform-docs.netlify.app>.
## Building documentation locally

We are using [Honkit](https://github.com/honkit/honkit), a fork of Gitbook, which CLI is now deprecated.

1. Clone this project 
1. Install Honkit `npm install --only=dev`
1. Preview your project: `npm run serve`
1. Add content and see the magic
