# Network Platform Documentation

This documentation aims at centralizing all the Network Platform documentation, including, for example, platform goals, design choices, and deployment adresses.

The documentation is under construction within this [repository](https://gitlab.com/juniorenterprises/network-platform-docs).

## Participating
This documentation is hosted as markdown files in a Gitlab repository. To join us: 

1. Create a [Gitlab](https://gitlab.com) account
2. Join our [discord server](https://discord.gg/hCgv59rNMB) OR send an email to one of the [maintainer/owner](https://gitlab.com/juniorenterprises/network-platform-docs/-/project_members)
3. You will then be added. A minimum knowledge of Git will be appreciated
